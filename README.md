This is a companion project for <https://docs.gitlab.com/ee/ci/ssh_keys/>.

If you are looking for the Alpine version, check this comment https://gitlab.com/gitlab-examples/ssh-private-key/issues/4#note_35042568.